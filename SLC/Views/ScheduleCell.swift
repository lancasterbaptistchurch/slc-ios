//
//  ScheduleCell.swift
//  SLC
//
//  Created by Joe Wheaton on 5/5/17.
//  Copyright © 2017 Lancaster Baptist Church. All rights reserved.
//

import UIKit

class ScheduleCell: UITableViewCell {
    @IBOutlet weak var fromTime: UILabel!
    @IBOutlet weak var toTime: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var room: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
