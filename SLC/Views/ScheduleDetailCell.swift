//
//  ScheduleDetailCell.swift
//  SLC
//
//  Created by Joe Wheaton on 5/8/17.
//  Copyright © 2017 Lancaster Baptist Church. All rights reserved.
//

import UIKit

class ScheduleDetailCell: UITableViewCell {

    @IBOutlet weak var sessionTitle: UILabel!
    @IBOutlet weak var sessionCombinedLabel: UILabel!
    @IBOutlet weak var trackLabel: UILabel!
    @IBOutlet weak var notesButton: UIButton!
    
    var notesURL = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    
    @IBAction func notesTouched(_ sender: Any) {
        
        UIApplication.shared.open(URL(string: self.notesURL)!, options: [:], completionHandler: nil)
        
    }
}
