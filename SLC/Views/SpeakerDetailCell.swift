//
//  SpeakerDetailCell.swift
//  SLC
//
//  Created by Joe Wheaton on 5/8/17.
//  Copyright © 2017 Lancaster Baptist Church. All rights reserved.
//

import UIKit

class SpeakerDetailCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeRoomLabel: UILabel!
    @IBOutlet weak var trackLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
