//
//  Extensions.swift
//  SLC
//
//  Created by Joe Wheaton on 5/12/17.
//  Copyright © 2017 Lancaster Baptist Church. All rights reserved.
//

import Foundation
import UIKit

extension UserDefaults {
    
    static func isFirstLaunchToday() -> Bool {
        
        let lastLaunchStr = "lastLaunchedStr"
//        let today = Date()
//        let f = DateFormatter()
//        f.dateFormat = "yyyy-MM-dd"
//        let todayStr = f.string(from: today)
        let todayStr = todayString()
        
        if let isFirstLaunchToday = UserDefaults.standard.string(forKey: lastLaunchStr) {
//            print("isFirstLaunchToday = \(isFirstLaunchToday)")
//            print("todayStr = \(todayStr)")
            if isFirstLaunchToday == todayStr {
                
                return false
                
            } else {
                
                UserDefaults.standard.set(todayStr, forKey: lastLaunchStr)
                UserDefaults.standard.synchronize()
                return true
                
            }
            
        } else {
            
            UserDefaults.standard.set(todayStr, forKey: lastLaunchStr)
            UserDefaults.standard.synchronize()
            return true
            
        }

    }
    
}

