//
//  Globals.swift
//  SLC
//
//  Created by Joe Wheaton on 5/5/17.
//  Copyright © 2017 Lancaster Baptist Church. All rights reserved.
//

import Foundation

var events = Conference()

func todayString() -> String {
    let today = Date()
    let f = DateFormatter()
    f.dateFormat = "yyyy-MM-dd"
    let todayStr = f.string(from: today)
    return todayStr
}
