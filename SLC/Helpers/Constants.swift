//
//  Constants.swift
//  SLC
//
//  Created by Joe Wheaton on 5/2/17.
//  Copyright © 2017 Lancaster Baptist Church. All rights reserved.
//

import Foundation
import UIKit

let EVENTS_URL = "https://slconference.com/api/v3/events"
let SPEAKERS_URL = "https://slconference.com/api/v3/speakers"
let SPEAKER_IMAGE_URL = "https://slconference.com/sites/default/files/thumbnails/image/"

typealias DownloadComplete = () -> ()

let greenColor = UIColor(red: 109/255, green: 192/255, blue: 76/255, alpha: 1)
