//
//  ConferenceAPI.swift
//  SLC
//
//  Created by Joe Wheaton on 1/16/18.
//  Copyright © 2018 Lancaster Baptist Church. All rights reserved.
//

import Foundation

final class ConferenceAPI {
    static let shared = ConferenceAPI()
    
    private init() {
        
    }
}
