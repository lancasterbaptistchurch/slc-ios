//
//  ConferenceSpeaker.swift
//  SLC
//
//  Created by Joe Wheaton on 5/4/17.
//  Copyright © 2017 Lancaster Baptist Church. All rights reserved.
//

import Foundation
import SwiftyJSON

class ConferenceSpeaker {
    
    var name : String = ""
    
    var nid : Int = 0
    
    var fname : String = ""
    
    var lname : String = ""
    
    var prefix : String = ""
    
    var bio : String = ""
    
    var location : String = ""
    
    var img : String = ""
    
    init() {}
    
    init(jsonData: JSON) {
        
        let s:JSON = jsonData
        
        if let name = s["name"].string {
            self.name = name
        }
        
        if let nid = s["nid"].int {
            self.nid = nid
        }
        
        if let fname = s["fname"].string {
            self.fname = fname
        }
        
        if let lname = s["lname"].string {
            self.lname = lname
        }
        
        if let prefix = s["prefix"].string {
            self.prefix = prefix
        }
        
        if let bio = s["bio"].string {
            self.bio = bio
        }
        
        if let location = s["location"].string {
            self.location = location
        }
        
        if let img = s["img"].string {
            self.img = img
        }
        
    }
    
}
