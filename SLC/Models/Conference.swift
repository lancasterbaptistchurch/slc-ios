//
//  Conference.swift
//  SLC
//
//  Created by Joe Wheaton on 5/2/17.
//  Copyright © 2017 Lancaster Baptist Church. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class Conference {
    
    public private(set) var days: [String: [ConferenceEvent]]
    public private(set) var sections: [String]
    public private(set) var sessions: [ConferenceEvent]
    public private(set) var speakers: [ConferenceSpeaker]
    
    init() {
        days = [:]
        sections = []
        sessions = []
        speakers = []
    }
    
    func getConferenceEvents(completed: @escaping DownloadComplete, owner: UIViewController) {
        Alamofire.request(EVENTS_URL).validate().responseJSON { (response) in
            let json = JSON(response.result.value as Any)
            //print(json)
            
            switch response.result {
                
            case .success:
                //print("success")
                self.days.removeAll()
                self.sessions.removeAll()
                for (_, subJson): (String, JSON) in json {
                    //println("subJson: ")
                    //println(subJson)
                    for (_, subsubJson): (String, JSON) in subJson {
                        //print("node: \(subsubJson["node"])")
                        if let node = subsubJson["node"] as? JSON {
                            //print("node = \(node)")
                            
                            let event = ConferenceEvent(jsonData: node)
                            
                            let day = event.day
                            if event.type == "Standard" || event.type == "Service" {
                                if self.days[day] != nil {
                                    self.days[day]!.append(event)
                                }
                                else {
                                    self.days[day] = [event]
                                }
                            }
                            
                            if event.type == "Session" {
                                self.sessions.append(event)
                            }
                        }
                    }
                    
                }
                //print(self.sessions)
                
            case .failure(let error):
                print("failure: \(error)")
                
                let alert = UIAlertController(title: "Error", message: "Could not download the conference schedule. Are you connected to the Internet?", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler: nil))
                owner.present(alert, animated: true, completion: nil)
            }
            
            completed()
        }
    }
    
    func getConferenceSpeakers(completed: @escaping DownloadComplete, owner: UIViewController) {
        
        Alamofire.request(SPEAKERS_URL).validate().responseJSON { (response) in
            let json = JSON(response.result.value as Any)
            
            switch response.result {
                
            case .success:
                self.speakers.removeAll()
                for (_, subJson): (String, JSON) in json {
                    for (_, subsubJson): (String, JSON) in subJson {
                        
                        if let node = subsubJson["node"] as? JSON {
                            let speaker = ConferenceSpeaker(jsonData: node)
                            
                            self.speakers.append(speaker)
                        }
                        
                        
                    }
                }
                
            case .failure(let error):
                print("failure: \(error)")
                
                let alert = UIAlertController(title: "Error", message: "Could not download the conference speakers. Are you connected to the Internet?", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler: nil))
                owner.present(alert, animated: true, completion: nil)
                
            }
            completed()
        }
        
    }
    
}
