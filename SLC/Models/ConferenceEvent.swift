//
//  ConferenceEvents.swift
//  SLC
//
//  Created by Joe Wheaton on 5/1/17.
//  Copyright © 2017 Lancaster Baptist Church. All rights reserved.
//

import Foundation
import SwiftyJSON

class ConferenceEvent {
    
    public private(set) var nid: Int
    public private(set) var type: String
    public private(set) var title: String
    public private(set) var room: String
    public private(set) var speaker: String
    public private(set) var download: String
    public private(set) var notes: String
    public private(set) var track: String
    public private(set) var weight: Int
    public private(set) var day: String
    public private(set) var from: String
    public private(set) var to: String
    
    init() {
        self.nid = 0
        self.type = ""
        self.title = ""
        self.room = ""
        self.speaker = ""
        self.download = "None"
        self.notes = "None"
        self.track = ""
        self.weight = 0
        self.day = ""
        self.from = ""
        self.to = ""
    }
    
    init(jsonData: JSON) {
        
        let n:JSON = jsonData
        
        if let nid = n["nid"].string {
            if nid != "" {
                self.nid = Int(nid)!
            }
            else {
                self.nid = 0
            }
        }
        else {
            self.nid = 0
        }
        
        if let type = n["type"].string {
            self.type = type
        }
        else {
            self.type = ""
        }
        
        if let title = n["title"].string {
            self.title = title
        }
        else {
            self.title = ""
        }
        
        if let room = n["room"].string {
            self.room = room
        }
        else {
            self.room = ""
        }
        
        if let speaker = n["speaker"].string {
            self.speaker = speaker
        }
        else {
            self.speaker = ""
        }
        
        if let download = n["download"].string {
            self.download = download
        }
        else {
            self.download = "None"
        }
        
        if let notes = n["notes"].string {
            self.notes = notes
        }
        else {
            self.notes = "None"
        }
        
        if let track = n["track"].string {
            self.track = track
        }
        else {
            self.track = ""
        }
        
        if let weight = n["weight"].string {
            if weight != "" {
                self.weight = Int(weight)!
            }
            else {
                self.weight = 0
            }
        }
        else {
            self.weight = 0
        }
        
        if let day = n["day"].string {
            self.day = day
        }
        else {
            self.day = ""
        }
        
        if let dateFrom = n["from"].string {
            self.from = dateFrom
        }
        else {
            self.from = ""
        }
        
        if let dateTo = n["to"].string {
            self.to = dateTo
        }
        else {
            self.to = ""
        }
        
    }
    
    init(nid: Int, type: String, title: String, room: String, speaker: String, download: String, notes: String, track: String, weight: Int, day: String, from: String, to: String) {
        self.nid = nid
        self.type = type
        self.title = title
        self.room = room
        self.speaker = speaker
        self.download = download
        self.notes = notes
        self.track = track
        self.weight = weight
        self.day = day
        self.from = from
        self.to = to
    }
    
    func prettyDay() -> String {
        
        let f = DateFormatter()
        f.dateFormat = "yyyy-M-d"
        if let dayDate = f.date(from: day) {
            f.dateFormat = "MMMM d"
            let retValue = f.string(from: dayDate)
            return retValue
        }
        
        return ""
    }
    
}
