//
//  MapDetailVC.swift
//  SLC
//
//  Created by Joe Wheaton on 5/4/17.
//  Copyright © 2017 Lancaster Baptist Church. All rights reserved.
//

import UIKit
import Firebase

class MapDetailVC: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var mapScrollView: UIScrollView!
    
    var mapTitle: String = ""
    var mapImage = UIImageView()
    var mapImageSize = CGSize(width: 0,height: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = mapTitle
        mapScrollView.delegate = self
        
        let mapImageName = mapTitle.lowercased().replacingOccurrences(of: " ", with: "-")
        
        mapImage.contentMode = UIViewContentMode.center
        
        addImageViewtoScrollView(mapImageName)
        
        Analytics.logEvent(AnalyticsEventViewItem, parameters: [
            AnalyticsParameterItemName: mapTitle,
            AnalyticsParameterItemCategory: "map"
            ]
        )
        
    }
    
    override func viewDidLayoutSubviews() {
        
        mapScrollView.maximumZoomScale = 2.0
        mapScrollView.contentSize = mapImageSize
        let widthScale = mapScrollView.bounds.size.width / mapImageSize.width
        let heightScale = mapScrollView.bounds.size.height / mapImageSize.height
        mapScrollView.minimumZoomScale = min(widthScale, heightScale)
        mapScrollView.setZoomScale(max(widthScale,heightScale), animated: false)
        
    }
    
    
    func addImageViewtoScrollView(_ nameOfMap: String) {
        
        let image = UIImage(named: nameOfMap)!
        mapImage.image = image
        mapImage.frame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
        mapImageSize = mapImage.frame.size
        mapScrollView.contentSize = mapImageSize
        
        centerScrollViewContents()
        
        mapScrollView.addSubview(mapImage)
        
    }
    
    func centerScrollViewContents() {
        
        let boundSize = mapScrollView.bounds.size
        var contentsFrame = mapImage.frame
        
        if contentsFrame.size.width < boundSize.width {
            contentsFrame.origin.x = (boundSize.width - contentsFrame.size.width) / 2
        }
        else {
            contentsFrame.origin.x = 0
        }
        
        if contentsFrame.size.height < boundSize.height {
            contentsFrame.origin.y = (boundSize.height - contentsFrame.size.height) / 2
        }
        else {
            contentsFrame.origin.y = 0
        }
        
        mapImage.frame = contentsFrame
        
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        
        return mapImage
        
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        
        centerScrollViewContents()
        
    }

}
