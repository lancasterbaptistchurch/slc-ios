//
//  MapVC.swift
//  SLC
//
//  Created by Joe Wheaton on 5/4/17.
//  Copyright © 2017 Lancaster Baptist Church. All rights reserved.
//

import UIKit

class MapVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var mapTableViewData = [
        "Auditorium Seating",
        "Campus",
        "Nursery",
        "Revels Floor 1",
        "Revels Floor 2",
        "Revels Floor 3",
        "West Wing Floor 1",
        "West Wing Floor 2",
        "Walther Center"]
    var activeMap = -1
    
    @IBOutlet var mapTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mapTableView.delegate = self
        mapTableView.dataSource = self

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return mapTableViewData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = mapTableView.dequeueReusableCell(withIdentifier: "MapCell", for: indexPath)
        cell.textLabel?.text = String(mapTableViewData[indexPath.row])
        cell.textLabel?.font = UIFont(name:"Avenir", size:20)
        cell.textLabel?.textColor = UIColor.darkGray
            
        return cell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        
        activeMap = indexPath.row
        
        return indexPath
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showMapDetail" {
            if let detailsVC = segue.destination as? MapDetailVC {
                
                detailsVC.mapTitle = mapTableViewData[activeMap]
                
            }
        }
        
    }

}
