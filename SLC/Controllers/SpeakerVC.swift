//
//  SpeakerVC.swift
//  SLC
//
//  Created by Joe Wheaton on 5/1/17.
//  Copyright © 2017 Lancaster Baptist Church. All rights reserved.
//

import UIKit
import VHUD

class SpeakerVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var speakersTableView: UITableView!
    
    var selectedSpeaker = ConferenceSpeaker()
    var filteredSpeakers = [ConferenceSpeaker]()
    var actInd = VHUDContent(.loop(3.0))
    
    let searchController = UISearchController(searchResultsController: nil)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        speakersTableView.delegate = self
        speakersTableView.dataSource = self
        
        actInd.shape = .circle
        actInd.loadingText = "Loading"
        actInd.completionText = "Done!"
        actInd.style = .light
        actInd.background = .blur(.dark)
        
        searchController.searchResultsUpdater = self as? UISearchResultsUpdating
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.tintColor = greenColor
        searchController.searchBar.barTintColor = greenColor
        searchController.searchBar.searchBarStyle = UISearchBarStyle.minimal
        definesPresentationContext = true
        speakersTableView.tableHeaderView = searchController.searchBar
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        getData()
        
    }
    
    @IBAction func refresh(_ sender: Any) {
        
       getData()
        
    }
    
    func getData() {
        
        VHUD.show(actInd)
        events.getConferenceSpeakers(completed: updateUI, owner: self)
        
    }
    
    func updateUI() {
        speakersTableView.reloadData()
        VHUD.dismiss(0.5)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredSpeakers.count
        }
        return events.speakers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = speakersTableView.dequeueReusableCell(withIdentifier: "SpeakerCell", for: indexPath) as! SpeakerCell
        let speaker: ConferenceSpeaker
        
        if searchController.isActive && searchController.searchBar.text != "" {
            
            speaker = filteredSpeakers[indexPath.row]
            
        } else {
            
            speaker = events.speakers[indexPath.row]
            
        }
        
        cell.speaker = speaker
        cell.speakerName.text = speaker.name

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if searchController.isActive && searchController.searchBar.text != "" {
            
            selectedSpeaker = filteredSpeakers[indexPath.row]
            
        } else {
            
            selectedSpeaker = events.speakers[indexPath.row]
            
        }

        performSegue(withIdentifier: "showSpeakerDetail", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showSpeakerDetail" {
            
            let vc = segue.destination as! SpeakerDetailVC
            
            vc.speaker = selectedSpeaker
            
        }
        
    }
    
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        filteredSpeakers = events.speakers.filter { speaker in
            return speaker.name.lowercased().contains(searchText.lowercased())
        }
        
        updateUI()
    }

}

extension SpeakerVC: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchText: searchController.searchBar.text!)
    }
    
}

