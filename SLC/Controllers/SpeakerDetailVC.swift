//
//  SpeakerDetailVC.swift
//  SLC
//
//  Created by Joe Wheaton on 5/5/17.
//  Copyright © 2017 Lancaster Baptist Church. All rights reserved.
//

import UIKit
import Kingfisher
import Firebase

class SpeakerDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var speakerImage: UIImageView!
    @IBOutlet weak var speakerName: UILabel!
    @IBOutlet weak var speakerLocation: UILabel!
    @IBOutlet weak var speakerBio: UILabel!
    @IBOutlet weak var speakerSessionsTable: UITableView!
    
    var speaker = ConferenceSpeaker()
    var speakerSessions: [ConferenceEvent] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        speakerSessionsTable.delegate = self
        speakerSessionsTable.dataSource = self
        
        speakerName.text = speaker.name
        speakerLocation.text = speaker.location
        speakerBio.text = speaker.bio
        
        let imageURL = URL(string: SPEAKER_IMAGE_URL + speaker.img)
        speakerImage.kf.setImage(with: imageURL)
        
        speakerSessions = events.sessions.filter({
            
            $0.speaker == self.speaker.name
            
        })
        if speakerSessions.count == 0 {
            speakerSessionsTable.isHidden = true
        }
        updateUI()
        
        Analytics.logEvent(AnalyticsEventViewItem, parameters: [
            AnalyticsParameterItemName: speaker.name,
            AnalyticsParameterItemCategory: "speaker"
            ]
        )
    }
    
    func updateUI() {
        
        speakerSessionsTable.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return speakerSessions.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = speakerSessionsTable.dequeueReusableCell(withIdentifier: "SpeakerDetailCell", for: indexPath) as! SpeakerDetailCell
        
        cell.titleLabel.text = speakerSessions[indexPath.row].title
        cell.timeRoomLabel.text = "\(speakerSessions[indexPath.row].prettyDay()), \(speakerSessions[indexPath.row].from) | \(speakerSessions[indexPath.row].room)"
        cell.trackLabel.text = speakerSessions[indexPath.row].track
        
        return UITableViewCell()
    }

}
