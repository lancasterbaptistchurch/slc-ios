//
//  ScheduleDetailVC.swift
//  SLC
//
//  Created by Joe Wheaton on 5/5/17.
//  Copyright © 2017 Lancaster Baptist Church. All rights reserved.
//

import UIKit
import Firebase
import VHUD

class ScheduleDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var sessionTable: UITableView!
    
    var splitSession = ConferenceEvent()
    var sessionFilter: [ConferenceEvent] = []
    var actInd = VHUDContent(.loop(3.0))
    
    override func viewDidLoad() {
        super.viewDidLoad()

        sessionTable.delegate = self
        sessionTable.dataSource = self
        
        actInd.shape = .circle
        actInd.loadingText = "Loading"
        actInd.completionText = "Done!"
        actInd.style = .light
        actInd.background = .blur(.dark)
        
        self.title = splitSession.title
        //print(splitSession)
        
        sessionFilter =  events.sessions.filter({
            
            $0.from == self.splitSession.from && $0.day == self.splitSession.day
            
        })
        sessionFilter.sort(by: { $0.weight < $1.weight})
        //print(sessionFilter)
        //print("Number of Sessions: \(sessionFilter.count)")
        updateUI()
        
        Analytics.logEvent(AnalyticsEventViewItem, parameters: [
            AnalyticsParameterItemName: splitSession.title,
            AnalyticsParameterItemCategory: "session"
            ]
        )
        
    }
    
    @IBAction func refreshTouched(_ sender: Any) {
        
        getData()
        
    }
    
    func getData() {
        
        VHUD.show(actInd)
        events.getConferenceEvents(completed: dataComplete, owner: self)
        
    }
    
    func dataComplete() {
        
        self.sessionFilter =  events.sessions.filter({
            
            $0.from == self.splitSession.from && $0.day == self.splitSession.day
            
        })
        self.sessionFilter.sort(by: { $0.weight < $1.weight})
        
        self.updateUI()
        VHUD.dismiss(0.5)
        
    }
    
    func updateUI() {
        
        sessionTable.reloadData()
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return sessionFilter.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = sessionTable.dequeueReusableCell(withIdentifier: "ScheduleDetailCell", for: indexPath) as! ScheduleDetailCell
        
        let session = sessionFilter[indexPath.row]
        
        cell.sessionTitle.text = session.title
        
        cell.sessionCombinedLabel.text = "\(session.room) | \(session.speaker)"
        
        cell.trackLabel.text = "\(session.track)"
        
        cell.notesURL = "\(session.notes)"
        
        if session.notes.isEmpty || session.notes.compare("None") == ComparisonResult.orderedSame {
            cell.notesButton.isHidden = true
        } else {
            cell.notesButton.isHidden = false
        }
        
        return cell
    }

}
