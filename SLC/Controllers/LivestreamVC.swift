//
//  LivestreamVC.swift
//  SLC
//
//  Created by Joe Wheaton on 5/5/17.
//  Copyright © 2017 Lancaster Baptist Church. All rights reserved.
//

import UIKit
import Firebase

class LivestreamVC: UIViewController {

    @IBOutlet weak var watchLiveButton: UIButton!
    @IBAction func watchLiveTouched(_ sender: Any) {
        
        Analytics.logEvent(AnalyticsEventViewItem, parameters: [
            AnalyticsParameterItemName: "Livestream",
            AnalyticsParameterItemCategory: "livestream"
            ]
        )
        
        UIApplication.shared.open(URL(string: "http://lbclive.tv")!, options: [:], completionHandler: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        watchLiveButton.layer.borderColor = greenColor.cgColor
        watchLiveButton.layer.borderWidth = 1
        watchLiveButton.layer.cornerRadius = 5
    }
    
    override func viewDidLayoutSubviews() {
        
        
        
    }

}
