//
//  ScheduleVC.swift
//  SLC
//
//  Created by Joe Wheaton on 5/1/17.
//  Copyright © 2017 Lancaster Baptist Church. All rights reserved.
//

import UIKit
import VHUD

class ScheduleVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var scheduleTableView: UITableView!
    @IBOutlet weak var scheduleDaySegment: UISegmentedControl!

    var eventDays = [String]()
    var selectedDay = 0
    var selectedEvent = ConferenceEvent()
    var firstRun = true
    var actInd = VHUDContent(.loop(3.0))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scheduleTableView.delegate = self
        scheduleTableView.dataSource = self
        
        actInd.shape = .circle
        actInd.loadingText = "Loading"
        actInd.completionText = "Done!"
        actInd.style = .light
        actInd.background = .blur(.dark)

        getData()
    }
    
    @IBAction func refresh(_ sender: Any) {
        
        getData()
        
    }
    
    func getData() {
        
        VHUD.show(actInd)
        events.getConferenceEvents(completed: dataComplete, owner: self)
        
    }
    
    func dataComplete() {
        self.eventDays = Array(events.days.keys).sorted()
        //print("eventDays: \(self.eventDays)")
        self.updateUI()
        VHUD.dismiss(0.5)
    }
    
    func updateUI() {
        
        if UserDefaults.isFirstLaunchToday() {
            let todayStr = todayString()
            if let index = eventDays.index(of: todayStr) {
                scheduleDaySegment.selectedSegmentIndex = index
                selectedDay = scheduleDaySegment.selectedSegmentIndex
            }
        }
        
        scheduleTableView.reloadData()
        scheduleTableView.setContentOffset(CGPoint.zero, animated: false)
    }
    
    @IBAction func scheduleDaySegmentTouched(_ sender: Any) {
        selectedDay = scheduleDaySegment.selectedSegmentIndex
        updateUI()
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if eventDays.count != 0 {
            if let numRows = events.days[eventDays[selectedDay]]?.count {
                //print(numRows)
                return numRows
            }
        }
        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = scheduleTableView.dequeueReusableCell(withIdentifier: "ScheduleCell", for: indexPath) as! ScheduleCell
        
        if let events = events.days[eventDays[selectedDay]] {
            cell.fromTime.text = events[indexPath.row].from
            cell.toTime.text = events[indexPath.row].to
            cell.title.text = events[indexPath.row].title
            cell.room.text = events[indexPath.row].room
            
            if (events[indexPath.row].title.range(of: "Split") != nil) {
                cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            }
            else {
                cell.accessoryType = UITableViewCellAccessoryType.none
            }
            
        }

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let days = events.days[eventDays[selectedDay]] {
            selectedEvent = days[indexPath.row]
            if let currentCell = scheduleTableView.cellForRow(at: indexPath) {
                if currentCell.accessoryType == UITableViewCellAccessoryType.disclosureIndicator {
                    performSegue(withIdentifier: "showScheduleDetail", sender: self)
                }
            }
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showScheduleDetail" {
            
            let vc = segue.destination as! ScheduleDetailVC
            
            vc.splitSession = selectedEvent
            
        }
        
    }
    
}

