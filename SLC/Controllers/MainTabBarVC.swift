//
//  MainTabBarVC.swift
//  SLC
//
//  Created by Joe Wheaton on 5/4/17.
//  Copyright © 2017 Lancaster Baptist Church. All rights reserved.
//

import UIKit

class MainTabBarVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
      
        let selectedColor = UIColor(red: 164/255, green: 225/255, blue: 129/255, alpha: 1)
        UITabBar.appearance().selectionIndicatorImage = UIImage().makeImageWithColorAndSize(color: selectedColor, size: CGSize(width: tabBar.frame.width/4, height: tabBar.frame.height))
        
        for item in self.tabBar.items! {
            if let image = item.image {
                item.image = image.withRenderingMode(.alwaysOriginal)
            }
        }
      
    }

}

extension UIImage {
    func makeImageWithColorAndSize(color: UIColor, size: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
        var image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
