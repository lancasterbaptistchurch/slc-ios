//
//  ConferenceEventTests.swift
//  SLC
//
//  Created by Joe Wheaton on 5/1/17.
//  Copyright © 2017 Lancaster Baptist Church. All rights reserved.
//

import XCTest
@testable import SLC

class ConferenceEventTests: XCTestCase {
    
    let expectedNid = 42
    let expectedType = "General"
    let expectedTitle = "How to Preach"
    let expectedRoom = "Worship Center"
    let expectedSpeaker = "Pastor Paul Chappell"
    let expectedDownload = "None"
    let expectedNotes = "None"
    let expectedTrack = "Missions"
    let expectedWeight = 1
    let expectedDay = "2017-06-10"
    let expectedFrom = "9:00 am"
    let expectedTo = "9:30 am"
    
    let expectedEmptyInt = 0
    let expectedEmptyString = ""
    let expectedEmptyNone = "None"

    var systemUnderTest: ConferenceEvent!
    var systemUnderTest2: ConferenceEvent!
    
    override func setUp() {
        super.setUp()

        systemUnderTest = ConferenceEvent(nid: expectedNid, type: expectedType, title: expectedTitle, room: expectedRoom, speaker: expectedSpeaker, download: expectedDownload, notes: expectedNotes, track: expectedTrack, weight: expectedWeight, day: expectedDay, from: expectedFrom, to: expectedTo)
        
        systemUnderTest2 = ConferenceEvent()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testSUT_initializesNid() {
        XCTAssertEqual(systemUnderTest.nid, expectedNid)
    }
    
    func testSUT_initializesType() {
        XCTAssertEqual(systemUnderTest.type, expectedType)
    }
    
    func testSUT_initializesTitle() {
        XCTAssertEqual(systemUnderTest.title, expectedTitle)
    }
    
    func testSUT_initializesRoom() {
        XCTAssertEqual(systemUnderTest.room, expectedRoom)
    }
    
    func testSUT_initializesSpeaker() {
        XCTAssertEqual(systemUnderTest.speaker, expectedSpeaker)
    }
    
    func testSUT_initializesDownload() {
        XCTAssertEqual(systemUnderTest.download, expectedDownload)
    }
    
    func testSUT_initializesNotes() {
        XCTAssertEqual(systemUnderTest.notes, expectedNotes)
    }
    
    func testSUT_initializesTrack() {
        XCTAssertEqual(systemUnderTest.track, expectedTrack)
    }
    
    func testSUT_initializesWeight() {
        XCTAssertEqual(systemUnderTest.weight, expectedWeight)
    }
    
    func testSUT_initializesDay() {
        XCTAssertEqual(systemUnderTest.day, expectedDay)
    }
    
    func testSUT_initializesFrom() {
        XCTAssertEqual(systemUnderTest.from, expectedFrom)
    }
    
    func testSUT_initializesTo() {
        XCTAssertEqual(systemUnderTest.to, expectedTo)
    }
    
    func testSUT2_initializesEmpty() {
        XCTAssertEqual(systemUnderTest2.nid, expectedEmptyInt)
        XCTAssertEqual(systemUnderTest2.type, expectedEmptyString)
        XCTAssertEqual(systemUnderTest2.title, expectedEmptyString)
        XCTAssertEqual(systemUnderTest2.room, expectedEmptyString)
        XCTAssertEqual(systemUnderTest2.speaker, expectedEmptyString)
        XCTAssertEqual(systemUnderTest2.download, expectedEmptyNone)
        XCTAssertEqual(systemUnderTest2.notes, expectedEmptyNone)
        XCTAssertEqual(systemUnderTest2.track, expectedEmptyString)
        XCTAssertEqual(systemUnderTest2.weight, expectedEmptyInt)
        XCTAssertEqual(systemUnderTest2.day, expectedEmptyString)
        XCTAssertEqual(systemUnderTest2.from, expectedEmptyString)
        XCTAssertEqual(systemUnderTest2.to, expectedEmptyString)
    }
    
}
