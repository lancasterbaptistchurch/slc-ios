//
//  ConferenceTests.swift
//  SLC
//
//  Created by Joe Wheaton on 5/1/17.
//  Copyright © 2017 Lancaster Baptist Church. All rights reserved.
//

import XCTest
@testable import SLC

class ConferenceTests: XCTestCase {
    
    let expectedEmptyConference: [String: [ConferenceEvent]] = [:]
    
    var systemUnderTest: Conference!
    
    override func setUp() {
        super.setUp()
        
        systemUnderTest = Conference()
    }
    
    override func tearDown() {
        super.tearDown()
    }

    // Tests go here :)
    func testSUT_initializesEmptyConference() {
        XCTAssertEqual(systemUnderTest.days.count, 0)
    }
    
    // 
    
    // addDay
    // test whether we can add a day to days
    
    
}
